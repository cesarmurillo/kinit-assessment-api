<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name',50);
			$table->integer('edad');
			$table->string('correo',50);
			$table->integer('id_tipo')->unsigned();
			$table->string('empresa',50);
			$table->integer('id_rubro')->unsigned();
			$table->integer('id_perfil')->unsigned();
        });
		Schema::table('usuarios', function ($table) {
   		 	$table->foreign('id_tipo')->references('id')->on('tipos');
			$table->foreign('id_rubro')->references('id')->on('rubros');
			$table->foreign('id_perfil')->references('id')->on('perfiles');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usuarios');
    }
}
