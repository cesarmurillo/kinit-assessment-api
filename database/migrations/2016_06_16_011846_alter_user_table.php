<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuarios', function ($table) {
    $table->dropColumn('id_perfil');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::table('usuarios', function ($table) {
    $table->integer('id_perfil')->unsigned();
    $table->foreign('id_perfil')->references('id')->on('perfiles');
});
    }
}
