<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('auth',10);
			$table->tinyInteger('contacto');
			for($i=1;$i<=20;$i++){
			$table->integer('data'.$i);
			}
			$table->integer('id_usuario')->unsigned();

        });
		
		Schema::table('data', function ($table) {
		
			$table->foreign('id_usuario')->references('id')->on('usuarios');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('data');
    }
}
