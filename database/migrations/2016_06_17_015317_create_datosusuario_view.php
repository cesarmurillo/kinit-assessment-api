<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatosusuarioView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE 
VIEW `kinit_app`.`datosusuario` AS
    SELECT 
        `d`.`auth` AS `auth`,
        `u`.`name` AS `name`,
        `u`.`correo` AS `correo`,
        `t`.`nombre` AS `tipo`,
        `r`.`nombre` AS `rubro`,
        `u`.`empresa` AS `empresa`,
        (((((((((`d`.`data1` + `d`.`data2`) + `d`.`data3`) + `d`.`data4`) + `d`.`data5`) + `d`.`data6`) + `d`.`data7`) + `d`.`data8`) + `d`.`data9`) + `d`.`data10`) AS `capacidad`,
        (((((((((`d`.`data11` + `d`.`data12`) + `d`.`data13`) + `d`.`data14`) + `d`.`data15`) + `d`.`data16`) + `d`.`data17`) + `d`.`data18`) + `d`.`data19`) + `d`.`data20`) AS `liderazgo`,
        `d`.`contacto` AS `contact`
    FROM
        (((`kinit_app`.`usuarios` `u`
        JOIN `kinit_app`.`data` `d` ON ((`d`.`id_usuario` = `u`.`id`)))
        JOIN `kinit_app`.`tipos` `t` ON ((`u`.`id_tipo` = `t`.`id`)))
        JOIN `kinit_app`.`rubros` `r` ON ((`u`.`id_rubro` = `t`.`id`)))');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
