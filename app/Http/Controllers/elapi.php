<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\data;
use Storage;

use App\usuario;
use DB;

class elapi extends Controller
{
    public function upload( $auth, Request $request){
    	$status=2;
		$message="unknown error";
 		$user= usuario::where('auth',$auth)->first();
		$res=array();
		if(!isset($user)){
    		$user=new usuario;
			$user->auth=$auth;
			$user->correo=$request->input('mail');
			$user->id_tipo=$request->input('type');
			$user->save();
			$data=$request->input('data');
			if(isset($data)){
				foreach($data as $dato){
					DB::table('respuestas')->insert(['id_usuario'=>$user->id,'id_pregunta'=>$dato['id'],'valor'=>$dato['data']]);
				}
				$regreso = DB::table('datosusuario')->where('auth', '=', $auth)->first();
				if(!empty($regreso)){
				$liderazgo=$regreso->liderazgo;
				$capacidad=$regreso->capacidad;
				$idperf=(($liderazgo>2 and $capacidad>2 )? "4": ($liderazgo>2 ? "2" :($capacidad>2 ? "3" : "1")));
				$perfil =DB::table('perfiles')->where('id',$idperf )->first();
				$res['profile']=$perfil->nombre;
				$res['description']=$perfil->descripcion;
				$message="data upload succesfull";
				$recomendaciones=DB::table('recomendaciones')->select('recomendacion','imagen')->where('id_perfil',$idperf)->get();
				$res['tips']=array();
				foreach($recomendaciones as $recomendacion){
					$tip['recomendacion']=$recomendacion->recomendacion;
					$imagen=$recomendacion->imagen;
					try{
						$tip['imagen']= 'data:image/jpeg ;base64,'.base64_encode(Storage::get('/ditmx/'.$imagen));
					}
					catch(Exception $e){
						$tip['imagen']='not found';
					}
					array_push($res['tips'],$tip);
				}	
				}else{
					$status=1;
					$message="DB error or not found";
				}
			}
		} else {
			$message="";
			if(null !==$request->input('sector')){
				$user->sector=$request->input('sector');
				$message="updated sector ";
			}
			if(null !==$request->input('mail')){
				$user->correo=$request->input('mail');
				$message=$message."updated mail ";
			}
			if(null !==$request->input('nombre')){
				$user->name=$request->input('nombre');
				$message=$message."updated nombre ";
			}
			if(null !==$request->input('tema')){
				$user->tema=$request->input('tema');
				$message=$message."updated tema ";
			}
			if(null !==$request->input('contacto')){
				$user->name=$request->input('contacto');
				$message=$message."updated mail  contacto";
			}
			if(null !==$request->input('data')){
				DB::table('respuestas')->where('id_usuario',$user->id)->delete();
				$message=$message."updated respuestas ";
				foreach($request->input('data') as $dato){
					DB::table('respuestas')->insert(['id_usuario'=>$user->id,'id_pregunta'=>$dato['id'],'valor'=>$dato['data']]);
				}
				$regreso = DB::table('datosusuario')->where('auth', '=', $auth)->first();
				if(!empty($regreso)){
					$liderazgo=$regreso->liderazgo;
					$capacidad=$regreso->capacidad;
					$idperf=(($liderazgo>2 and $capacidad>2 )? "4": ($liderazgo>2 ? "2" :($capacidad>2 ? "3" : "1")));
					$perfil =DB::table('perfiles')->where('id',$idperf )->first();
					$res['profile']=$perfil->nombre;
					$res['description']=$perfil->descripcion;
					$message=$message."data upload succesfull";
					$recomendaciones=DB::table('recomendaciones')->select('recomendacion','imagen')->where('id_perfil',$idperf)->get();
					$res['tips']=array();
				foreach($recomendaciones as $recomendacion){
					$tip['recomendacion']=$recomendacion->recomendacion;
					$imagen=$recomendacion->imagen;
					try{
						$tip['imagen']='data:image/jpeg ;base64,'.base64_encode(Storage::get('/ditmx/'.$imagen));
					}
					catch(Exception $e){
						$tip['imagen']='not found';
					}
					array_push($res['tips'],$tip);
				}
				}
			}
			$user->save();
		}
	
			$res['status']=$status;
			$res['message']=$message;
		return response()->json($res);
    }
	
	public function fetch($auth,Request $request){
		$status=2;
		$message="unknown error";
		$res=array();
		$regreso=DB::table('datosusuario')->where('auth','=',$auth)->first();
		if(!empty($regreso)){
			$liderazgo=$regreso->liderazgo;
			$capacidad=$regreso->capacidad;
			$idperf=($liderazgo>2 and $capacidad>2 )? "4": $liderazgo>2 ? "3" :$capacidad>2 ? "2" : "1";
			$perfil =DB::table('perfiles')->where('id', '=',$idperf )->value('nombre');
			$regreso->perfil=$perfil;
			$message="request succesfull";
			$res['data']=$regreso;
		}else{
			$status=1;
			$message="data not found";
		}
		$res['status']=$status;
		$res['message']=$message;
		return response()->json($res);
	}
	
	
	public function questions($type){
		$status=2;
		$message="unknown error";
		$res=array();
		if($type>0){
			$status=0;
			$message="success";
			$res['questions']=DB::table('preguntas')->where('id_tipo', '=',$type)->get();
		} else{
			$status=1;
			$message="bad request";
		}
		$res['message']=$message;
		$res['status']=$status;
		return response()->json($res);
	}
	public function s3try(){
		return Storage::files('ditmx');
	}
}
