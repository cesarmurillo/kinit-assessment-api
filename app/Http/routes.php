<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers:Origin, Content-Type, Accept, Authorization, X-Requested-With');

Route::get('/', function () {
    return view('welcome');
});

Route::post('/api/upload/{auth}','elapi@upload');
Route::get('/api/lead/{auth}','elapi@fetch');
Route::get('/api/questions/{type}','elapi@questions');
Route::get('/api/prueba','elapi@s3try');


